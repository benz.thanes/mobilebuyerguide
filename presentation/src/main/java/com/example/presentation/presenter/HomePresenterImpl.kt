package com.example.presentation.presenter

import com.example.data.entity.model.MobileModelDisplay
import com.example.domain.usecase.GetFavoriteDataUseCase
import com.example.presentation.contractor.HomeContractor
import com.example.presentation.util.SortUtil

class HomePresenterImpl constructor(private val getFavoriteDataUseCase: GetFavoriteDataUseCase) : HomeContractor.Presenter {

    lateinit var view: HomeContractor.View

    override fun provideView(view: HomeContractor.View) {
        this.view = view
    }

    override fun loadFavoriteListStorage() {
        getFavoriteDataUseCase.getFavoriteData().let {
            view.setFavoriteListStorage(it)
        }
    }


    override fun sortMobileList(mobileList: ArrayList<MobileModelDisplay>, favList: ArrayList<MobileModelDisplay>, position: Int) {
        val sortedMobileList = SortUtil.sortListCondition(mobileList, position)
        val sortedFavList = SortUtil.sortListCondition(favList, position)
        view.setDataSort(sortedMobileList, sortedFavList)
    }

    override fun reloadFavList(favList: ArrayList<MobileModelDisplay>, position: Int) {
        val sortedFavList = SortUtil.sortListCondition(favList, position)
        view.setReloadFavList(sortedFavList)
    }

    override fun removeFavDataInMobileList(mobileList: ArrayList<MobileModelDisplay>, mobileItemRemove: MobileModelDisplay) {
        var position = 0
        for (i in 0..mobileList.size) {
            if (mobileList[i].id == mobileItemRemove.id) {
                mobileList[i].isFav = false
                position = i
                break
            }
        }
        view.setReloadRemoveFavList(mobileList, mobileItemRemove, position)
    }
}