package com.example.presentation.presenter

import com.example.data.entity.model.MobileModelDisplay
import com.example.domain.usecase.UpdateFavoriteListUseCase
import com.example.presentation.contractor.MobileFavoriteContractor


class MobileFavoriteFragmentPresenterImpl constructor(private val updateFavoriteListUseCase: UpdateFavoriteListUseCase) : MobileFavoriteContractor.Presenter {

    lateinit var view: MobileFavoriteContractor.View

    override fun provideView(view: MobileFavoriteContractor.View) {
        this.view = view
    }

    override fun getMobilesData(mobilesList: ArrayList<MobileModelDisplay>) {
        if (mobilesList.isNotEmpty()) {
            view.updateViewData(mobilesList)
        } else {
            view.updateViewDataEmpty()
        }
    }

    override fun updateFavoriteListStorage(mobilesFavList: ArrayList<MobileModelDisplay>) {
        updateFavoriteListUseCase.updateFavorite(mobilesFavList)
    }

}