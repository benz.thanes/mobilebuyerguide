package com.example.presentation.presenter

import android.annotation.SuppressLint
import com.example.data.entity.model.MobileDetailImageListModelDisplay
import com.example.domain.usecase.GetDetailImageListUseCase
import com.example.presentation.contractor.MobileDetailContractor
import io.reactivex.observers.DisposableObserver


class MobileDetailActivityPresenterImpl constructor(private val getDetailImageListUseCase: GetDetailImageListUseCase) : MobileDetailContractor.Presenter {

    lateinit var view: MobileDetailContractor.View

    override fun provideView(view: MobileDetailContractor.View) {
        this.view = view
    }

    @SuppressLint("CheckResult")
    override fun getMobileImage(id: String) {
//        getDetailImageListUseCase.execute(
//                onSuccess = {
//                    if (it.isNotEmpty()) {
//                        view.setMobileImage(MobileDetailModelMapper.mobileDetailImageListUI(ArrayList(it)))
//                    } else {
//                        view.setLoadImageFail()
//                    }
//                },
//                onError = { view.setLoadImageFail() },
//                params = id
//        )
        getDetailImageListUseCase.execute(object : DisposableObserver<List<MobileDetailImageListModelDisplay>>() {
            override fun onComplete() {

            }

            override fun onNext(t: List<MobileDetailImageListModelDisplay>) {
                view.setMobileImage(ArrayList(t))
            }

            override fun onError(e: Throwable) {
                view.setLoadImageFail()
            }
        }, id)
    }
}