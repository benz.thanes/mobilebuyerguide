package com.example.presentation.presenter

import android.annotation.SuppressLint
import com.example.data.entity.model.MobileModelDisplay
import com.example.domain.usecase.GetFavoriteDataUseCase
import com.example.domain.usecase.GetMobileListUseCase
import com.example.domain.usecase.UpdateFavoriteListUseCase
import com.example.presentation.contractor.MobileReviewContractor
import io.reactivex.observers.DisposableObserver


class MobileReviewFragmentPresenterImpl constructor(private val getMobileListUseCase: GetMobileListUseCase,
                                                    private val getFavoriteDataUseCase: GetFavoriteDataUseCase,
                                                    private val updateFavoriteListUseCase: UpdateFavoriteListUseCase) : MobileReviewContractor.Presenter {


    lateinit var view: MobileReviewContractor.View

    override fun provideView(view: MobileReviewContractor.View) {
        this.view = view
    }

    override fun loadFavoriteListStorage(loadData: Boolean): ArrayList<MobileModelDisplay> {
        getFavoriteDataUseCase.getFavoriteData().let {
            if (loadData) {
                view.setFavoriteListStorage(it)
            }
            return it
        }
    }

    @SuppressLint("CheckResult")
    override fun getMobilesData(favList: ArrayList<MobileModelDisplay>) {
//        getMobileListUseCase.execute(
//                onSuccess = {
//                    if (it.isNotEmpty()) {
//                        view.updateViewData(MobileModelMapper.mapMobileListModelUI(ArrayList(it), favList))
//                    } else {
//                        view.updateViewDataEmpty()
//                    }
//                },
//                onError = { view.updateViewDataEmpty() },
//                params = "")

        getMobileListUseCase.execute(object : DisposableObserver<List<MobileModelDisplay>>() {

            override fun onComplete() {

            }

            override fun onNext(t: List<MobileModelDisplay>) {
                view.updateViewData(ArrayList(t))
            }

            override fun onError(e: Throwable) {
                view.updateViewDataEmpty()
            }
        }, favList)
    }

    override fun updateFavoriteListStorage(mobilesFavList: ArrayList<MobileModelDisplay>) {
        updateFavoriteListUseCase.updateFavorite(mobilesFavList)
    }

}