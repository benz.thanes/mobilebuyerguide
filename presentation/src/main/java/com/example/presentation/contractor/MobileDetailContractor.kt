package com.example.presentation.contractor

import com.example.data.entity.model.MobileDetailImageListModelDisplay

interface MobileDetailContractor {
    interface View {
        fun setMobileImage(mobileDetailImageListModel: ArrayList<MobileDetailImageListModelDisplay>)
        fun setLoadImageFail()
    }

    interface Presenter {
        fun provideView(view: View)
        fun getMobileImage(id: String)
    }
}