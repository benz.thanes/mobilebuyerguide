package com.example.presentation.contractor

import com.example.data.entity.model.MobileModelDisplay

interface HomeContractor {
    interface View {
        fun setDataSort(sortedMobileList: ArrayList<MobileModelDisplay>, sortedFavList: ArrayList<MobileModelDisplay>)
        fun setReloadFavList(sortedFavList: ArrayList<MobileModelDisplay>)
        fun setReloadRemoveFavList(mobileList: ArrayList<MobileModelDisplay>, mobileItem: MobileModelDisplay, position: Int)
        fun setFavoriteListStorage(mobileFavList: ArrayList<MobileModelDisplay>)

    }

    interface Presenter {
        fun loadFavoriteListStorage()
        fun sortMobileList(mobileList: ArrayList<MobileModelDisplay>, favList: ArrayList<MobileModelDisplay>, position: Int)
        fun reloadFavList(favList: ArrayList<MobileModelDisplay>, position: Int)
        fun removeFavDataInMobileList(mobileList: ArrayList<MobileModelDisplay>, mobileItemRemove: MobileModelDisplay)
        fun provideView(view: View)
    }
}