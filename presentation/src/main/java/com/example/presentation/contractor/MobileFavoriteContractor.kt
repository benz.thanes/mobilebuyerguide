package com.example.presentation.contractor

import com.example.data.entity.model.MobileModelDisplay

interface MobileFavoriteContractor {


    interface View {
        fun updateViewData(mobilesList: ArrayList<MobileModelDisplay>)
        fun updateViewDataEmpty()
    }

    interface Presenter {
        fun provideView(view: View)
        fun getMobilesData(mobilesList: ArrayList<MobileModelDisplay>)
        fun updateFavoriteListStorage(mobilesFavList: ArrayList<MobileModelDisplay>)
    }
}