package com.example.presentation.contractor

import com.example.data.entity.model.MobileModelDisplay

interface MobileReviewContractor {

    interface View {
        fun updateViewData(mobilesList: ArrayList<MobileModelDisplay>?)
        fun updateViewDataEmpty()
        fun setFavoriteListStorage(mobileFavList: ArrayList<MobileModelDisplay>)
    }

    interface Presenter {
        fun provideView(view: View)
        fun getMobilesData(favList: ArrayList<MobileModelDisplay>)
        fun updateFavoriteListStorage(mobilesFavList: ArrayList<MobileModelDisplay>)
        fun loadFavoriteListStorage(loadData: Boolean = false): ArrayList<MobileModelDisplay>
    }

}