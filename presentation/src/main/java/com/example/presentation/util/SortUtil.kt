package com.example.presentation.util

import com.example.data.entity.model.MobileModelDisplay

object SortUtil {
    fun sortListCondition(items: ArrayList<MobileModelDisplay>, position: Int): ArrayList<MobileModelDisplay> {
        return items.let { it ->
            when (position) {
                0 -> {
                    ArrayList(it.sortedWith(compareBy { it.price?.toDouble() }))
                }
                1 -> {
                    ArrayList(it.sortedWith(compareByDescending { it.price?.toDouble() }))
                }
                2 -> {
                    ArrayList(it.sortedWith(compareByDescending { it.rating?.toDouble() }))
                }
                else -> ArrayList(it)
            }
        }
    }
}