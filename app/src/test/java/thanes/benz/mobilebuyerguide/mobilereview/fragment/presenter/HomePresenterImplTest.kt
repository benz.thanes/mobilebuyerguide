package thanes.benz.mobilebuyerguide.mobilereview.fragment.presenter

import com.example.data.entity.model.MobileModelDisplay
import com.example.domain.usecase.GetFavoriteDataUseCase
import com.example.presentation.contractor.HomeContractor
import com.example.presentation.presenter.HomePresenterImpl
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import thanes.benz.mobilebuyerguide.BaseUnitTest

class HomePresenterImplTest : BaseUnitTest() {
    @Mock
    private lateinit var view: HomeContractor.View
    @Mock
    private lateinit var getFavoriteDataUseCase: GetFavoriteDataUseCase


    private lateinit var presenter: HomeContractor.Presenter

    override fun setUp() {
        super.setUp()
        presenter = HomePresenterImpl(getFavoriteDataUseCase)
        presenter.provideView(view)
    }

    @Test
    fun loadFavoriteListStorage() {
        Mockito.`when`(getFavoriteDataUseCase.getFavoriteData()).thenReturn(mobileModelListToTest)
        presenter.loadFavoriteListStorage()
        Mockito.verify(view).setFavoriteListStorage(mobileModelListToTest)
    }

    @Test
    fun sortMobileListLowToHigh() {
        presenter.sortMobileList(mobileModelListToTest, mobileModelListToTest, 0)
        Mockito.verify(view).setDataSort(mobileModelListLowToHigh, mobileModelListLowToHigh)
    }

    @Test
    fun sortMobileListLowHighToLow() {
        presenter.sortMobileList(mobileModelListToTest, mobileModelListToTest, 1)
        Mockito.verify(view).setDataSort(mobileModelListHighToLow, mobileModelListHighToLow)
    }

    @Test
    fun sortMobileListRating() {
        presenter.sortMobileList(mobileModelListToTest, mobileModelListToTest, 2)
        Mockito.verify(view).setDataSort(mobileModelListRating, mobileModelListRating)
    }

    @Test
    fun reloadFavList() {
        presenter.reloadFavList(mobileModelListToTest, -1)
        Mockito.verify(view).setReloadFavList(mobileModelListToTest)
    }

    @Test
    fun removeFavDataInMobileList() {
        presenter.removeFavDataInMobileList(mobileModelListToTest, MobileModelDisplay
        (id = "1", rating = "4.9", price = "179"))
        Mockito.verify(view).setReloadRemoveFavList(mobileModelListRemove, MobileModelDisplay
        (id = "1", rating = "4.9", price = "179"), 0)
    }
}