package thanes.benz.mobilebuyerguide.mobilereview.fragment.presenter

import com.example.data.entity.model.MobileModelDisplay
import com.example.domain.usecase.GetFavoriteDataUseCase
import com.example.domain.usecase.GetMobileListUseCase
import com.example.domain.usecase.UpdateFavoriteListUseCase
import com.example.presentation.contractor.MobileReviewContractor
import com.example.presentation.presenter.MobileReviewFragmentPresenterImpl
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.observers.DisposableObserver
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import thanes.benz.mobilebuyerguide.BaseUnitTest

class MobileReviewFragmentPresenterImplTest : BaseUnitTest() {
    @Mock
    private lateinit var view: MobileReviewContractor.View
    @Mock
    private lateinit var getMobileListUseCase: GetMobileListUseCase
    @Mock
    private lateinit var getFavoriteDataUseCase: GetFavoriteDataUseCase
    @Mock
    private lateinit var updateFavoriteListUseCase: UpdateFavoriteListUseCase

    private lateinit var presenter: MobileReviewContractor.Presenter

    override fun setUp() {
        super.setUp()
        presenter = MobileReviewFragmentPresenterImpl(getMobileListUseCase, getFavoriteDataUseCase, updateFavoriteListUseCase)
        presenter.provideView(view)
    }

    @Test
    fun getMobilesDataSuccess() {
        val argumentCaptor = argumentCaptor<DisposableObserver<List<MobileModelDisplay>>>()
        presenter.getMobilesData(mobileModelListToTest)
        verify(getMobileListUseCase).execute(argumentCaptor.capture(), anyOrNull())
        argumentCaptor.firstValue.onNext(mobileModelListService)
        Mockito.verify(view).updateViewData(mobileModelListService)
    }

    @Test
    fun getMobilesDataError() {
        val argumentCaptor = argumentCaptor<DisposableObserver<List<MobileModelDisplay>>>()
        presenter.getMobilesData(mobileModelListToTest)
        verify(getMobileListUseCase).execute(argumentCaptor.capture(), anyOrNull())
        argumentCaptor.firstValue.onError(Throwable())
        Mockito.verify(view).updateViewDataEmpty()
    }

    @Test
    fun updateFavoriteListStorage() {
        presenter.updateFavoriteListStorage(mobileModelListToTest)
    }

    @Test
    fun loadFavoriteListStorage() {
        Mockito.`when`(getFavoriteDataUseCase.getFavoriteData()).thenReturn(mobileModelListToTest)
        presenter.loadFavoriteListStorage()
        view.setFavoriteListStorage(mobileModelListToTest)
    }

}