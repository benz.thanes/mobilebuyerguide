package thanes.benz.mobilebuyerguide.mobilereview.fragment.presenter

import com.example.domain.usecase.UpdateFavoriteListUseCase
import com.example.presentation.contractor.MobileFavoriteContractor
import com.example.presentation.presenter.MobileFavoriteFragmentPresenterImpl
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import thanes.benz.mobilebuyerguide.BaseUnitTest

class MobileMobileFavoriteFragmentPresenterImplTest : BaseUnitTest() {
    @Mock
    private lateinit var view: MobileFavoriteContractor.View
    @Mock
    private lateinit var updateFavoriteListUseCase: UpdateFavoriteListUseCase

    private lateinit var presenter: MobileFavoriteContractor.Presenter

    override fun setUp() {
        super.setUp()
        presenter = MobileFavoriteFragmentPresenterImpl(updateFavoriteListUseCase)
        presenter.provideView(view)
    }

    @Test
    fun getMobilesData() {
        presenter.getMobilesData(mobileModelListToTest)
        Mockito.verify(view).updateViewData(mobileModelListToTest)
    }

    @Test
    fun updateFavoriteListStorage() {
        presenter.updateFavoriteListStorage(mobileModelListToTest)
    }

    @Test
    fun getMobilesDataEmpty() {
        presenter.getMobilesData(mobileModelListEmpty)
        Mockito.verify(view).updateViewDataEmpty()
    }
}