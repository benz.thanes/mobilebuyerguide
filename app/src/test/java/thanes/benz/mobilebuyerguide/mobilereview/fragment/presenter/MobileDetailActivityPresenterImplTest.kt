package thanes.benz.mobilebuyerguide.mobilereview.fragment.presenter

import com.example.data.entity.model.MobileDetailImageListModelDisplay
import com.example.domain.usecase.GetDetailImageListUseCase
import com.example.presentation.contractor.MobileDetailContractor
import com.example.presentation.presenter.MobileDetailActivityPresenterImpl
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.observers.DisposableObserver
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import thanes.benz.mobilebuyerguide.BaseUnitTest


class MobileDetailActivityPresenterImplTest : BaseUnitTest() {
    @Mock
    private lateinit var view: MobileDetailContractor.View
    @Mock
    private lateinit var getDetailImageListUseCase: GetDetailImageListUseCase

    private lateinit var presenter: MobileDetailContractor.Presenter

    override fun setUp() {
        super.setUp()
        presenter = MobileDetailActivityPresenterImpl(getDetailImageListUseCase)
        presenter.provideView(view)
    }

    @Test
    fun getMobileImageSuccess() {
        val argumentCaptor = argumentCaptor<DisposableObserver<List<MobileDetailImageListModelDisplay>>>()
        presenter.getMobileImage("")
        verify(getDetailImageListUseCase).execute(argumentCaptor.capture(), Mockito.anyString())
        argumentCaptor.firstValue.onNext(mobileDetailImageListDisplay)
        verify(view).setMobileImage(mobileDetailImageListDisplay)
    }


    @Test
    fun getMobileImageError() {
        val argumentCaptor = argumentCaptor<DisposableObserver<List<MobileDetailImageListModelDisplay>>>()
        presenter.getMobileImage("")
        verify(getDetailImageListUseCase).execute(argumentCaptor.capture(), Mockito.anyString())
        argumentCaptor.firstValue.onError(Throwable())
        verify(view).setLoadImageFail()
    }


}