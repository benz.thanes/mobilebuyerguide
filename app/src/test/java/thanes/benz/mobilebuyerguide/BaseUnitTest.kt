package thanes.benz.mobilebuyerguide

import com.example.data.entity.model.MobileDetailImageListModel
import com.example.data.entity.model.MobileDetailImageListModelDisplay
import com.example.data.entity.model.MobileModel
import com.example.data.entity.model.MobileModelDisplay
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.mockito.MockitoAnnotations

open class BaseUnitTest {

    lateinit var gson: Gson

    protected var mobileModelListService = ArrayList<MobileModelDisplay>()
    protected var mobileModelListToTest = ArrayList<MobileModelDisplay>()
    protected var mobileModelListEmpty = ArrayList<MobileModelDisplay>()
    protected var mobileModelListLowToHigh = ArrayList<MobileModelDisplay>()
    protected var mobileModelListHighToLow = ArrayList<MobileModelDisplay>()
    protected var mobileModelListRating = ArrayList<MobileModelDisplay>()
    protected var mobileModelListRemove = ArrayList<MobileModelDisplay>()

    protected var mobileDetailImageListDisplay = ArrayList<MobileDetailImageListModelDisplay>()

    @Before
    open fun setUp() {
        gson = Gson()
        MockitoAnnotations.initMocks(this)
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        setData()
    }

    private fun readJson(jsonName: String, mobileModel: Boolean, display: Boolean = true): Any {
        val json = this.javaClass.classLoader.getResourceAsStream(jsonName)
        val jsonString = convertStreamToString(json)
        return if (mobileModel) {
            when (display) {
                true -> gson.fromJson(jsonString, object : TypeToken<ArrayList<MobileModelDisplay>>() {}.type)
                false -> gson.fromJson(jsonString, object : TypeToken<ArrayList<MobileModel>>() {}.type)
            }
        } else {
            when (display) {
                true -> gson.fromJson(jsonString, object : TypeToken<ArrayList<MobileDetailImageListModelDisplay>>() {}.type)
                false -> gson.fromJson(jsonString, object : TypeToken<ArrayList<MobileDetailImageListModel>>() {}.type)
            }
        }

    }

    private fun convertStreamToString(`is`: java.io.InputStream): String {
        val s = java.util.Scanner(`is`).useDelimiter("\\A")
        return if (s.hasNext()) s.next() else ""
    }

    private fun setData() {
        mobileDetailImageListDisplay = readJson("mobile/mobilesImageList.json", false) as ArrayList<MobileDetailImageListModelDisplay>

        mobileModelListService = readJson("mobile/mobiles.json", true) as ArrayList<MobileModelDisplay>
        mobileModelListToTest.add(MobileModelDisplay(id = "1", rating = "4.9", price = "179", isFav = true))
        mobileModelListToTest.add(MobileModelDisplay(id = "2", rating = "1.0", price = "179.99", isFav = true))
        mobileModelListToTest.add(MobileModelDisplay(id = "3", rating = "5.0", price = "180", isFav = true))

        mobileModelListLowToHigh.add(MobileModelDisplay(id = "1", rating = "4.9", price = "179", isFav = true))
        mobileModelListLowToHigh.add(MobileModelDisplay(id = "2", rating = "1.0", price = "179.99", isFav = true))
        mobileModelListLowToHigh.add(MobileModelDisplay(id = "3", rating = "5.0", price = "180", isFav = true))

        mobileModelListHighToLow.add(MobileModelDisplay(id = "3", rating = "5.0", price = "180", isFav = true))
        mobileModelListHighToLow.add(MobileModelDisplay(id = "2", rating = "1.0", price = "179.99", isFav = true))
        mobileModelListHighToLow.add(MobileModelDisplay(id = "1", rating = "4.9", price = "179", isFav = true))

        mobileModelListRating.add(MobileModelDisplay(id = "3", rating = "5.0", price = "180", isFav = true))
        mobileModelListRating.add(MobileModelDisplay(id = "1", rating = "4.9", price = "179", isFav = true))
        mobileModelListRating.add(MobileModelDisplay(id = "2", rating = "1.0", price = "179.99", isFav = true))

        mobileModelListRemove.add(MobileModelDisplay(id = "1", rating = "4.9", price = "179", isFav = false))
        mobileModelListRemove.add(MobileModelDisplay(id = "2", rating = "1.0", price = "179.99", isFav = true))
        mobileModelListRemove.add(MobileModelDisplay(id = "3", rating = "5.0", price = "180", isFav = true))

    }

    @After
    open fun reset() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
    }
}