package thanes.benz.mobilebuyerguide.util

import com.example.presentation.util.SortUtil
import org.junit.Assert.assertEquals
import org.junit.Test
import thanes.benz.mobilebuyerguide.BaseUnitTest

class SortUtilTest : BaseUnitTest() {

    @Test
    fun sortListCondition() {
        assertEquals(SortUtil.sortListCondition(mobileModelListToTest, 0), mobileModelListLowToHigh)
        assertEquals(SortUtil.sortListCondition(mobileModelListToTest, 1), mobileModelListHighToLow)
        assertEquals(SortUtil.sortListCondition(mobileModelListToTest, 2), mobileModelListRating)
    }
}