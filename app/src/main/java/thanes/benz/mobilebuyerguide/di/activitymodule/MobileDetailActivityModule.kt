package thanes.benz.mobilebuyerguide.di.activitymodule

import com.example.domain.usecase.GetDetailImageListUseCase
import com.example.presentation.contractor.MobileDetailContractor
import com.example.presentation.presenter.MobileDetailActivityPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import thanes.benz.mobilebuyerguide.mobilereview.activity.MobileDetailActivity

@Module
abstract class MobileDetailActivityModule {

    @Binds
    abstract fun activity(activity: MobileDetailActivity): MobileDetailActivity

    @Module
    companion object {
        @JvmStatic
        @Provides
        fun provideMobileDetailActivityPresenter(getDetailImageListUseCase: GetDetailImageListUseCase): MobileDetailContractor.Presenter {
            return MobileDetailActivityPresenterImpl(getDetailImageListUseCase)
        }
    }
}