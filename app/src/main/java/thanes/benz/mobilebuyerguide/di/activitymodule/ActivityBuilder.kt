package thanes.benz.mobilebuyerguide.di.activitymodule

import dagger.Module
import dagger.android.ContributesAndroidInjector
import thanes.benz.mobilebuyerguide.di.activitymodule.HomeActivityModule
import thanes.benz.mobilebuyerguide.mobilereview.activity.HomeActivity
import thanes.benz.mobilebuyerguide.mobilereview.activity.MobileDetailActivity

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = [HomeActivityModule::class])
    abstract fun contributeHomeActivity(): HomeActivity

    @ContributesAndroidInjector(modules = [MobileDetailActivityModule::class])
    abstract fun contributeMobileDetailActivity(): MobileDetailActivity

}