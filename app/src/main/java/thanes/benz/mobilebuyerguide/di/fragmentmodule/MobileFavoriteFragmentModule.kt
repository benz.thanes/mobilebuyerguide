package thanes.benz.mobilebuyerguide.di.fragmentmodule

import com.example.domain.usecase.UpdateFavoriteListUseCase
import com.example.presentation.contractor.MobileFavoriteContractor
import com.example.presentation.presenter.MobileFavoriteFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import thanes.benz.mobilebuyerguide.mobilereview.fragment.MobileFavoriteFragment

@Module
abstract class MobileFavoriteFragmentModule {

    @Binds
    abstract fun fragment(fragment: MobileFavoriteFragment): MobileFavoriteFragment

    @Module
    companion object {
        @JvmStatic
        @Provides
        fun provideMobileFavoriteFragmentPresenter(updateFavoriteListUseCase: UpdateFavoriteListUseCase): MobileFavoriteContractor.Presenter {
            return MobileFavoriteFragmentPresenterImpl(updateFavoriteListUseCase)
        }
    }
}