package thanes.benz.mobilebuyerguide.di.fragmentmodule

import com.example.domain.usecase.GetFavoriteDataUseCase
import com.example.domain.usecase.GetMobileListUseCase
import com.example.domain.usecase.UpdateFavoriteListUseCase
import com.example.presentation.contractor.MobileReviewContractor
import com.example.presentation.presenter.MobileReviewFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import thanes.benz.mobilebuyerguide.mobilereview.fragment.MobileReviewFragment

@Module
abstract class MobileReviewFragmentModule {

    @Binds
    abstract fun fragment(fragment: MobileReviewFragment): MobileReviewFragment

    @Module
    companion object {
        @JvmStatic
        @Provides
        fun provideMobileReviewFragmentPresenter(getMobileListUseCase: GetMobileListUseCase,
                                                 getFavoriteDataUseCase: GetFavoriteDataUseCase,
                                                 updateFavoriteListUseCase: UpdateFavoriteListUseCase): MobileReviewContractor.Presenter {

            return MobileReviewFragmentPresenterImpl(getMobileListUseCase, getFavoriteDataUseCase, updateFavoriteListUseCase)
        }
    }
}