package thanes.benz.mobilebuyerguide.di.fragmentmodule

import dagger.Module
import dagger.android.ContributesAndroidInjector
import thanes.benz.mobilebuyerguide.di.fragmentmodule.MobileFavoriteFragmentModule
import thanes.benz.mobilebuyerguide.mobilereview.fragment.MobileFavoriteFragment
import thanes.benz.mobilebuyerguide.mobilereview.fragment.MobileReviewFragment

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = [MobileReviewFragmentModule::class])
    abstract fun contributeMobileReviewFragment(): MobileReviewFragment

    @ContributesAndroidInjector(modules = [MobileFavoriteFragmentModule::class])
    abstract fun contributeMobileFavoriteFragment(): MobileFavoriteFragment

}