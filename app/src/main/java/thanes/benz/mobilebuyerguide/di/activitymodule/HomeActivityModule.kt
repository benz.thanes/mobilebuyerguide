package thanes.benz.mobilebuyerguide.di.activitymodule

import com.example.domain.usecase.GetFavoriteDataUseCase
import com.example.presentation.contractor.HomeContractor
import com.example.presentation.presenter.HomePresenterImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import thanes.benz.mobilebuyerguide.mobilereview.activity.HomeActivity


@Module
abstract class HomeActivityModule {

    @Binds
    abstract fun activity(activity: HomeActivity): HomeActivity

    @Module
    companion object {
        @JvmStatic
        @Provides
        fun provideHomeActivityPresenter(getFavoriteDataUseCase: GetFavoriteDataUseCase): HomeContractor.Presenter {
            return HomePresenterImpl(getFavoriteDataUseCase)
        }
    }
}