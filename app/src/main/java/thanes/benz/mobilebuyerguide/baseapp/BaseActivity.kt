package thanes.benz.mobilebuyerguide.baseapp

import android.app.Dialog
import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity
import thanes.benz.mobilebuyerguide.util.DialogUtil

abstract class BaseActivity : DaggerAppCompatActivity() {

    private var dialog: Dialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog = DialogUtil.progressDialog(this)
    }

    fun showProgressDialog() {
        dialog?.show()
    }

    fun hideProgressDialog() {
        dialog?.let { DialogUtil.hideProgress(it) }
    }

}