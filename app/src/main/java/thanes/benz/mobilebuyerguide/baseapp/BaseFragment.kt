package thanes.benz.mobilebuyerguide.baseapp

import android.app.Dialog
import android.os.Bundle
import dagger.android.support.DaggerFragment
import thanes.benz.mobilebuyerguide.util.DialogUtil

abstract class BaseFragment : DaggerFragment() {
    private var dialog: Dialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context?.let { dialog = DialogUtil.progressDialog(it) }

    }

    fun showProgressDialog() {
        dialog?.show()
    }

    fun hideProgressDialog() {
        dialog?.let { DialogUtil.hideProgress(it) }
    }
}
