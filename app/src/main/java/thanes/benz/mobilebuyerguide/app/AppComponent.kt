package thanes.benz.mobilebuyerguide.app

import android.app.Application
import com.example.data.di.ServiceModule
import com.example.data.di.SharePreferenceModule
import com.example.domain.di.UseCaseModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import thanes.benz.mobilebuyerguide.di.activitymodule.ActivityBuilder
import thanes.benz.mobilebuyerguide.di.fragmentmodule.FragmentBuilder
import javax.inject.Singleton


@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,
    ActivityBuilder::class,
    FragmentBuilder::class,
    ServiceModule::class,
    UseCaseModule::class,
    SharePreferenceModule::class
]
)

interface AppComponent : AndroidInjector<DaggerApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(myApplication: MyApplication)
}