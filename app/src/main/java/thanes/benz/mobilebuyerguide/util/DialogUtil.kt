package thanes.benz.mobilebuyerguide.util

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import thanes.benz.mobilebuyerguide.R

object DialogUtil {
    private var listener: DialogListener? = null

    fun showSingleChoiceDialog(context: Context, items: Array<String>, default: Int) {
        lateinit var dialog: AlertDialog
        val builder = AlertDialog.Builder(context)
        builder.setTitle("")
        builder.setSingleChoiceItems(items, default) { _, position ->
            listener?.onClickItem(position)
            dialog.dismiss()

        }
        dialog = builder.create()
        dialog.show()
    }


    fun setDialogListener(listener: DialogListener? = null) {
        this.listener = listener
    }

    interface DialogListener {
        fun onClickItem(position: Int)
    }


    fun progressDialog(context: Context): Dialog {
        val dialog = Dialog(context)
        val inflate = LayoutInflater.from(context).inflate(R.layout.progress_dialog_layout, null)
        dialog.setContentView(inflate)
        dialog.setCancelable(false)
        dialog.window!!.setBackgroundDrawable(
                ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    fun hideProgress(dialog: Dialog?) {
        if (dialog != null && dialog.isShowing) {
            dialog.dismiss()
        }
    }
}


