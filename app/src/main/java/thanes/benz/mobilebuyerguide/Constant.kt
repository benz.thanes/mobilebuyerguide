package thanes.benz.mobilebuyerguide

object Constant {
    const val ARGUMENT_MOBILE_ITEM = "mobile"
    val ARRAY_SORT_LIST = arrayOf("Price low to high", "Price high to low", "Rating 5-1")
}