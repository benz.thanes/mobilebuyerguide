package thanes.benz.mobilebuyerguide.mobilereview.activity

import android.os.Bundle
import android.support.v4.view.ViewPager
import com.example.data.entity.model.MobileModelDisplay
import com.example.presentation.contractor.HomeContractor
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.toolbar_home.*
import thanes.benz.mobilebuyerguide.Constant.ARRAY_SORT_LIST
import thanes.benz.mobilebuyerguide.R
import thanes.benz.mobilebuyerguide.baseapp.BaseActivity
import thanes.benz.mobilebuyerguide.mobilereview.adapter.ShowImageListAdapter
import thanes.benz.mobilebuyerguide.mobilereview.fragment.MobileFavoriteFragment
import thanes.benz.mobilebuyerguide.mobilereview.fragment.MobileReviewFragment
import thanes.benz.mobilebuyerguide.util.DialogUtil
import javax.inject.Inject


class HomeActivity : BaseActivity(), HomeContractor.View {

    @Inject
    lateinit var presenter: HomeContractor.Presenter

    private var typeSort = -1
    private var mobileReviewFragment: MobileReviewFragment? = null
    private var mobileFavoriteFragment: MobileFavoriteFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        presenter.provideView(this)
        initView()
    }

    private fun initView() {
        mobileReviewFragment = MobileReviewFragment.newInstance()
        mobileReviewFragment?.setListener(reloadFavListener)
        mobileFavoriteFragment = MobileFavoriteFragment.newInstance()
        mobileFavoriteFragment?.setListener(swipeListener)
        val myPagerAdapter = ShowImageListAdapter(applicationContext, supportFragmentManager, mobileReviewFragment!!, mobileFavoriteFragment!!)
        viewPager.adapter = myPagerAdapter
        viewPager.addOnPageChangeListener(pagerListener)
        tabLayout.setupWithViewPager(viewPager)
        btn_toolbar_filter.setOnClickListener {
            DialogUtil.setDialogListener(object : DialogUtil.DialogListener {
                override fun onClickItem(position: Int) {
                    sortMobileList(position)
                    typeSort = position
                }
            })
            DialogUtil.showSingleChoiceDialog(this, ARRAY_SORT_LIST, typeSort)
        }
    }

    override fun setFavoriteListStorage(mobileFavList: ArrayList<MobileModelDisplay>) {
        mobileReviewFragment?.let {
            presenter.reloadFavList(mobileFavList, typeSort)
        }
    }

    fun sortMobileList(position: Int) {
        mobileReviewFragment?.let {
            presenter.sortMobileList(it.getMobilesList(), it.getFavMobilesList(), position)
        }
    }

    override fun setDataSort(sortedMobileList: ArrayList<MobileModelDisplay>, sortedFavList: ArrayList<MobileModelDisplay>) {
        mobileReviewFragment?.let {
            it.reLoadList(sortedMobileList)
            it.setFavList(sortedFavList)
        }
        mobileFavoriteFragment?.reLoadList(sortedFavList)
    }

    override fun setReloadFavList(sortedFavList: ArrayList<MobileModelDisplay>) {
        mobileFavoriteFragment?.reLoadList(sortedFavList)
    }

    private val pagerListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {

        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

        }

        override fun onPageSelected(position: Int) {
            when (position) {
                1 -> presenter.loadFavoriteListStorage()
            }
        }
    }

    private val swipeListener = object : MobileFavoriteFragment.ItemListener {
        override fun onItemOnSwipeToRemoveClick(mobileItem: MobileModelDisplay) {
            val mobileList = mobileReviewFragment!!.getMobilesList()
            presenter.removeFavDataInMobileList(mobileList, mobileItem)
        }
    }

    private val reloadFavListener = object : MobileReviewFragment.ItemListener {
        override fun onFavClick() {
            presenter.loadFavoriteListStorage()
        }
    }

    override fun setReloadRemoveFavList(mobileList: ArrayList<MobileModelDisplay>, mobileItem: MobileModelDisplay, position: Int) {
        mobileReviewFragment?.let {
            val favList = mobileReviewFragment?.getFavMobilesList()
            favList?.let { f ->
                f.remove(mobileItem)
                it.setFavList(f)
                it.reloadListWhenSwipeRemoveFav(position)
            }
        }
    }
}







