package thanes.benz.mobilebuyerguide.mobilereview.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.data.entity.model.MobileModelDisplay
import com.example.presentation.contractor.MobileReviewContractor
import kotlinx.android.synthetic.main.fragment_mobiles_list.*
import thanes.benz.mobilebuyerguide.Constant
import thanes.benz.mobilebuyerguide.R
import thanes.benz.mobilebuyerguide.baseapp.BaseFragment
import thanes.benz.mobilebuyerguide.mobilereview.activity.MobileDetailActivity
import thanes.benz.mobilebuyerguide.mobilereview.adapter.MobilesReviewAdapter
import thanes.benz.mobilebuyerguide.mobilereview.adapter.MobilesReviewAdapter.ItemListener
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList as ArrayList1


class MobileReviewFragment : BaseFragment(), MobileReviewContractor.View, ItemListener {

    @Inject
    lateinit var presenter: MobileReviewContractor.Presenter

    private var favMobilesList: ArrayList<MobileModelDisplay> = ArrayList()
    private var mobilesList: ArrayList<MobileModelDisplay> = ArrayList()

    private var listener: ItemListener? = null

    companion object {
        fun newInstance(): MobileReviewFragment {
            val args = Bundle()
            val fragment = MobileReviewFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mobiles_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.provideView(this)
        context?.let {
            showProgressDialog()
        }
        presenter.loadFavoriteListStorage(loadData = true)

    }

    override fun setFavoriteListStorage(mobileFavList: ArrayList<MobileModelDisplay>) {
        favMobilesList = mobileFavList
        presenter.getMobilesData(mobileFavList)
    }

    override fun updateViewDataEmpty() {
        tv_empty_data.visibility = View.VISIBLE
        hideProgressDialog()
    }

    override fun updateViewData(mobilesList: ArrayList<MobileModelDisplay>?) {
        tv_empty_data.visibility = View.GONE
        mobilesList?.let {
            this.mobilesList = it
            recyclerView_mobiles.layoutManager = LinearLayoutManager(activity)
            recyclerView_mobiles.adapter = MobilesReviewAdapter(it, this)
        }
        hideProgressDialog()
    }


    fun reLoadList(items: ArrayList<MobileModelDisplay>) {
        tv_empty_data.visibility = View.GONE
        this.mobilesList = items
        recyclerView_mobiles.adapter = MobilesReviewAdapter(mobilesList, this)
    }

    fun reloadListWhenSwipeRemoveFav(position: Int) {
        recyclerView_mobiles.adapter.notifyItemChanged(position, false)
    }

    override fun onFavClick(position: Int, fav: Boolean) {
        recyclerView_mobiles.adapter.notifyItemChanged(position, false)
        val mobileModel = mobilesList[position]
        if (fav) {
            favMobilesList.add(mobileModel)
        } else {
            removeFav(mobileModel)
        }
        presenter.updateFavoriteListStorage(favMobilesList)
        listener?.onFavClick()
    }

    private fun removeFav(mobileModelUI: MobileModelDisplay) {
        for (fav in favMobilesList) {
            if (fav.id == mobileModelUI.id) {
                favMobilesList.remove(fav)
                break
            }
        }
    }

    override fun onItemClick(mobileItem: MobileModelDisplay) {
        val intent = Intent(context, MobileDetailActivity::class.java)
        intent.putExtra(Constant.ARGUMENT_MOBILE_ITEM, mobileItem)
        startActivity(intent)
    }


    fun getMobilesList(): ArrayList<MobileModelDisplay> {
        return mobilesList
    }

    fun getFavMobilesList(): ArrayList<MobileModelDisplay> {
        return presenter.loadFavoriteListStorage()
    }

    fun setFavList(items: ArrayList<MobileModelDisplay>) {
        this.favMobilesList = items
    }

    fun setListener(listener: ItemListener) {
        this.listener = listener
    }

    interface ItemListener {
        fun onFavClick()
    }
}



