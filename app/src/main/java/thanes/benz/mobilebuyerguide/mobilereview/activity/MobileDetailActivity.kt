package thanes.benz.mobilebuyerguide.mobilereview.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.example.data.entity.model.MobileDetailImageListModelDisplay
import com.example.data.entity.model.MobileModelDisplay
import com.example.presentation.contractor.MobileDetailContractor
import kotlinx.android.synthetic.main.activity_mobile_detail.*
import kotlinx.android.synthetic.main.toolbar_detail.*
import thanes.benz.mobilebuyerguide.Constant
import thanes.benz.mobilebuyerguide.R
import thanes.benz.mobilebuyerguide.baseapp.BaseActivity
import thanes.benz.mobilebuyerguide.mobilereview.adapter.MobileImageAdapter
import javax.inject.Inject

class MobileDetailActivity : BaseActivity(), MobileDetailContractor.View {


    @Inject
    lateinit var presenter: MobileDetailContractor.Presenter

    private var mobileModel = MobileModelDisplay()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile_detail)
        restoreFromBundle(savedInstanceState)
        initToolbar()
        initView()
        presenter.provideView(this)
        mobileModel.id?.let {
            showProgressDialog()
            presenter.getMobileImage(it)
        }

    }

    private fun initToolbar() {
        btn_toolbar_back.setImageResource(R.drawable.ic_baseline_arrow_back_24)
        btn_toolbar_back.setOnClickListener { onBackPressed() }
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        tv_title.text = mobileModel.name
        tv_brand.text = mobileModel.brand
        tv_detail.text = mobileModel.description

        val titlePrice = getString(R.string.price)
        val price = mobileModel.price
        tv_price.text = "$titlePrice$price"

        val titleRating = getString(R.string.rating)
        val rating = mobileModel.rating
        tv_rating.text = "$titleRating$rating"
    }

    private fun restoreFromBundle(savedInstanceState: Bundle?) {
        val bundle = savedInstanceState ?: intent.extras
        if (bundle != null) {
            mobileModel = bundle.getParcelable(Constant.ARGUMENT_MOBILE_ITEM)
        }
    }


    override fun setMobileImage(mobileDetailImageListModel: ArrayList<MobileDetailImageListModelDisplay>) {
        tv_empty_data.visibility = View.GONE
        viewpager_img_mobile.adapter = MobileImageAdapter(this, mobileDetailImageListModel)
        hideProgressDialog()
    }

    override fun setLoadImageFail() {
        tv_empty_data.visibility = View.VISIBLE
        hideProgressDialog()

    }

}
