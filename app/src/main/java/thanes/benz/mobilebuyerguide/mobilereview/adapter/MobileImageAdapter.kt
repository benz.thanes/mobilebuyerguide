package thanes.benz.mobilebuyerguide.mobilereview.adapter

import android.content.Context
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.data.entity.model.MobileDetailImageListModelDisplay
import thanes.benz.mobilebuyerguide.R


//class MobileImageAdapter(private val items: List<MobileDetailImageListModelDisplay>) : RecyclerView.Adapter<MobileImageAdapter.ViewHolder>() {
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_mobile_image, parent, false))
//    }
//
//    override fun getItemCount() = items.size
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.bind(items[position])
//    }
//
//    class ViewHolder(itemsView: View) : RecyclerView.ViewHolder(itemsView) {
//        fun bind(mobileModel: MobileDetailImageListModelDisplay) {
//            itemView.apply {
//                Glide.with(this).load(mobileModel.url).into(img_mobile)
//            }
//        }
//    }
//}

class MobileImageAdapter constructor(context: Context, private val items: List<MobileDetailImageListModelDisplay>) : PagerAdapter() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.item_mobile_image, view, false)!!
        val imageView = imageLayout.findViewById(R.id.img_mobile) as ImageView
        Glide.with(view).load(items[position].url).into(imageView)
        view.addView(imageLayout, 0)

        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

}