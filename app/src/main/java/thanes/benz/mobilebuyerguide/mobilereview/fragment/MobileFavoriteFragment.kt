package thanes.benz.mobilebuyerguide.mobilereview.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.data.entity.model.MobileModelDisplay
import com.example.presentation.contractor.MobileFavoriteContractor
import kotlinx.android.synthetic.main.fragment_mobiles_list.*
import thanes.benz.mobilebuyerguide.Constant
import thanes.benz.mobilebuyerguide.R
import thanes.benz.mobilebuyerguide.baseapp.BaseFragment
import thanes.benz.mobilebuyerguide.mobilereview.activity.MobileDetailActivity
import thanes.benz.mobilebuyerguide.mobilereview.adapter.MobileFavoriteAdapter
import thanes.benz.mobilebuyerguide.util.SwipeToDeleteCallback
import javax.inject.Inject


class MobileFavoriteFragment : BaseFragment(), MobileFavoriteContractor.View, MobileFavoriteAdapter.ItemListener {

    @Inject
    lateinit var presenter: MobileFavoriteContractor.Presenter

    private var listener: ItemListener? = null
    private var favList: ArrayList<MobileModelDisplay>? = null
    private var swipeHandler: SwipeToDeleteCallback? = null

    companion object {
        fun newInstance(): MobileFavoriteFragment {
            val args = Bundle()
            val fragment = MobileFavoriteFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mobiles_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.provideView(this)
        initView()
    }

    private fun initView() {
        recyclerView_mobiles.layoutManager = LinearLayoutManager(activity)
        context?.let {
            swipeHandler = object : SwipeToDeleteCallback(it) {
                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    favList?.let { favList ->
                        listener?.onItemOnSwipeToRemoveClick(favList[viewHolder.adapterPosition])
                        favList.remove(favList[viewHolder.adapterPosition])
                        presenter.updateFavoriteListStorage(favList)
                        recyclerView_mobiles.adapter.notifyDataSetChanged()
                    }
                    if (recyclerView_mobiles.adapter.itemCount == 0) {
                        tv_empty_data.visibility = View.VISIBLE
                    }
                }
            }
            val itemTouchHelper = ItemTouchHelper(swipeHandler)
            itemTouchHelper.attachToRecyclerView(recyclerView_mobiles)
        }
    }


    override fun updateViewDataEmpty() {
        tv_empty_data.visibility = View.VISIBLE
        recyclerView_mobiles.adapter?.notifyDataSetChanged()
        recyclerView_mobiles.visibility = View.INVISIBLE
    }

    override fun updateViewData(mobilesList: ArrayList<MobileModelDisplay>) {
        tv_empty_data.visibility = View.GONE
        recyclerView_mobiles.visibility = View.VISIBLE
        recyclerView_mobiles.adapter = MobileFavoriteAdapter(mobilesList, this)
    }

    override fun onItemClick(mobileItem: MobileModelDisplay) {
        val intent = Intent(context, MobileDetailActivity::class.java)
        intent.putExtra(Constant.ARGUMENT_MOBILE_ITEM, mobileItem)
        startActivity(intent)
    }

    fun reLoadList(items: ArrayList<MobileModelDisplay>) {
        favList = items
        presenter.getMobilesData(items)
    }

    interface ItemListener {
        fun onItemOnSwipeToRemoveClick(mobileItem: MobileModelDisplay)
    }

    fun setListener(listener: ItemListener) {
        this.listener = listener
    }

}
