package thanes.benz.mobilebuyerguide.mobilereview.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import thanes.benz.mobilebuyerguide.R
import thanes.benz.mobilebuyerguide.mobilereview.fragment.MobileFavoriteFragment
import thanes.benz.mobilebuyerguide.mobilereview.fragment.MobileReviewFragment


class ShowImageListAdapter(private var context: Context, fm: FragmentManager,
                           private var mobileReviewFragment: MobileReviewFragment,
                           private var mobileFavoriteFragment: MobileFavoriteFragment) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                mobileReviewFragment
            }
            1 -> {
                mobileFavoriteFragment
            }
            else -> {
                Fragment()
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> context.getString(R.string.mobile_list)
            1 -> context.getString(R.string.favorite_list)
            else -> {
                return ""
            }
        }
    }
}