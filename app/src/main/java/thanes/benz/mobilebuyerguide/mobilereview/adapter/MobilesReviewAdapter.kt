package thanes.benz.mobilebuyerguide.mobilereview.adapter

import android.annotation.SuppressLint
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.data.entity.model.MobileModelDisplay
import kotlinx.android.synthetic.main.item_mobile_review_layout.view.*
import thanes.benz.mobilebuyerguide.R

class MobilesReviewAdapter(private var items: ArrayList<MobileModelDisplay>, private val listener: ItemListener) : RecyclerView.Adapter<MobilesReviewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_mobile_review_layout, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
        holder.itemView.setOnClickListener {
            listener.onItemClick(items[position])
        }
        holder.itemView.icon_fav.apply {
            setOnClickListener {
                if (items[position].isFav!!) {
                    items[position].isFav = false
                    icon_fav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_baseline_favorite_24px))
                } else {
                    items[position].isFav = true
                    icon_fav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_baseline_favorite_border_24px))
                }
                listener.onFavClick(position, items[position].isFav!!)
            }
        }
    }

    class ViewHolder(itemsView: View) : RecyclerView.ViewHolder(itemsView) {
        @SuppressLint("SetTextI18n")
        fun bind(mobile: MobileModelDisplay) {
            itemView.apply {
                tv_title.text = mobile.name ?: ""
                tv_detail.text = mobile.description ?: ""

                // set price
                val titlePrice = context.getString(R.string.price)
                val price = mobile.price
                tv_price.text = "$titlePrice $price"

                //set rating
                val titleRating = context.getString(R.string.rating)
                val rating = mobile.rating
                tv_rating.text = "$titleRating $rating"

                if (mobile.isFav!!) {
                    icon_fav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_baseline_favorite_24px))
                } else {
                    icon_fav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_baseline_favorite_border_24px))
                }
                Glide.with(this).load(mobile.thumbImageURL).into(img_mobile)
            }
        }
    }

    interface ItemListener {
        fun onFavClick(position: Int, fav: Boolean)
        fun onItemClick(mobileItem: MobileModelDisplay)
    }

}