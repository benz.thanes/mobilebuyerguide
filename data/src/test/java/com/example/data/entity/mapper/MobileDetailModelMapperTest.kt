package com.example.data.entity.mapper

import com.example.data.entity.model.MobileDetailImageListModel
import com.example.data.entity.model.MobileDetailImageListModelDisplay
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MobileDetailModelMapperTest {
    private var mobileDetailImageListModel = ArrayList<MobileDetailImageListModel>()
    private var mobileDetailImageListModelMapDisplay = ArrayList<MobileDetailImageListModelDisplay>()


    @Before
    fun setUp() {
        mobileDetailImageListModel.add(MobileDetailImageListModel(id = "1", mobile_id = "1", url = "www.91-img.com/gallery_images_uploads/f/c/fc3fba717874d64cf15d30e77a16617a1e63cc0b.jpg"))
        mobileDetailImageListModel.add(MobileDetailImageListModel(id = "6", mobile_id = "1", url = "https://www.91-img.com/gallery_images_uploads/b/4/b493185e7767c2a99cfeef712b11377f625766f2.jpg"))
        mobileDetailImageListModel.add(MobileDetailImageListModel(id = "7", mobile_id = "1", url = "https://www.91-img.com/gallery_images_uploads/c/3/c32cff8945621ad06c929f50af9f7c55f978c726.jpg"))

        mobileDetailImageListModelMapDisplay.add(MobileDetailImageListModelDisplay(id = "1", mobile_id = "1", url = "https://www.91-img.com/gallery_images_uploads/f/c/fc3fba717874d64cf15d30e77a16617a1e63cc0b.jpg"))
        mobileDetailImageListModelMapDisplay.add(MobileDetailImageListModelDisplay(id = "6", mobile_id = "1", url = "https://www.91-img.com/gallery_images_uploads/b/4/b493185e7767c2a99cfeef712b11377f625766f2.jpg"))
        mobileDetailImageListModelMapDisplay.add(MobileDetailImageListModelDisplay(id = "7", mobile_id = "1", url = "https://www.91-img.com/gallery_images_uploads/c/3/c32cff8945621ad06c929f50af9f7c55f978c726.jpg"))
    }

    @Test
    fun mapMobileListModelUI() {
        //Check list after mapper is equal
        assertEquals(MobileDetailModelMapper.mobileDetailImageListUI(mobileDetailImageListModel), mobileDetailImageListModelMapDisplay)
    }

    @Test
    fun checkMapImageListModelUI() {
        //Check Map image url when url not have http
        assertEquals(MobileDetailModelMapper.mobileDetailImageListUI(mobileDetailImageListModel)[0].url, mobileDetailImageListModelMapDisplay[0].url)
    }


}