package com.example.data.entity.mapper

import com.example.data.entity.model.MobileModel
import com.example.data.entity.model.MobileModelDisplay
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MobileModelMapperTest {
    private var mobileListModelList = ArrayList<MobileModel>()
    private var mobileDetModelMapDisplayList = ArrayList<MobileModelDisplay>()
    private var mobileDetModelMapDisplayListFav = ArrayList<MobileModelDisplay>()


    @Before
    fun setUp() {

        // mock not fav
        mobileListModelList.add(MobileModel(id = "1", rating = "4.9", price = "179"))
        mobileListModelList.add(MobileModel(id = "2", rating = "1.0", price = "179.99"))
        mobileListModelList.add(MobileModel(id = "3", rating = "5.0", price = "180"))

        mobileDetModelMapDisplayList.add(MobileModelDisplay(id = "1", rating = "4.9", price = "179", isFav = false, name = "", brand = "", thumbImageURL = "", description = ""))
        mobileDetModelMapDisplayList.add(MobileModelDisplay(id = "2", rating = "1.0", price = "179.99", isFav = false, name = "", brand = "", thumbImageURL = "", description = ""))
        mobileDetModelMapDisplayList.add(MobileModelDisplay(id = "3", rating = "5.0", price = "180", isFav = false, name = "", brand = "", thumbImageURL = "", description = ""))


        //mock fav
        mobileDetModelMapDisplayListFav.add(MobileModelDisplay(id = "1", rating = "4.9", price = "179", isFav = true, name = "", brand = "", thumbImageURL = "", description = ""))
        mobileDetModelMapDisplayListFav.add(MobileModelDisplay(id = "2", rating = "1.0", price = "179.99", isFav = true, name = "", brand = "", thumbImageURL = "", description = ""))
        mobileDetModelMapDisplayListFav.add(MobileModelDisplay(id = "3", rating = "5.0", price = "180", isFav = true, name = "", brand = "", thumbImageURL = "", description = ""))

    }

    @Test
    fun mapMobileListModelUI() {
        assertEquals(MobileModelMapper.mapMobileListModelUI(mobileListModelList, arrayListOf()), mobileDetModelMapDisplayList)
    }

    @Test
    fun mapMobileListModeHaveFavoriteUI() {
        val listFavMapper = MobileModelMapper.mapMobileListModelUI(mobileListModelList, mobileDetModelMapDisplayListFav)
        assertEquals(listFavMapper, mobileDetModelMapDisplayListFav)
    }
}