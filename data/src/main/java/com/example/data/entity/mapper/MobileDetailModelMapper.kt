package com.example.data.entity.mapper

import com.example.data.entity.model.MobileDetailImageListModel
import com.example.data.entity.model.MobileDetailImageListModelDisplay

object MobileDetailModelMapper {

    private const val HTTP = "http"
    private const val PRE_HTTPS = "https://"

    fun mobileDetailImageListUI(mobileDetailList: List<MobileDetailImageListModel>): List<MobileDetailImageListModelDisplay> {
        val mobileDetailModelUiList = ArrayList<MobileDetailImageListModelDisplay>()
        for (mobileDetailModel in mobileDetailList) {
            mobileDetailModelUiList.add(mapImageListModelUI(mobileDetailModel))
        }
        return mobileDetailModelUiList
    }

    private fun mapImageListModelUI(mobileDetailModel: MobileDetailImageListModel): MobileDetailImageListModelDisplay {
        return MobileDetailImageListModelDisplay(
                id = mobileDetailModel.id ?: "",
                mobile_id = mobileDetailModel.mobile_id ?: "",
                url = mobileDetailModel.url?.let { url ->
                    if (url.contains(HTTP)) {
                        url
                    } else {
                        "$PRE_HTTPS$url"
                    }
                }
        )
    }
}