package com.example.data.entity.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MobileModelDisplay(
        var brand: String? = null,
        var thumbImageURL: String? = null,
        var price: String? = null,
        var name: String? = null,
        var rating: String? = null,
        var id: String? = null,
        var description: String? = null,
        var isFav: Boolean? = false
) : Parcelable