package com.example.data.entity.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MobileDetailImageListModel(
        @SerializedName("id") var id: String? = null,
        @SerializedName("mobile_id") var mobile_id: String? = null,
        @SerializedName("url") var url: String? = null
) : Parcelable
