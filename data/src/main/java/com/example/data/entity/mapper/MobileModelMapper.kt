package com.example.data.entity.mapper

import com.example.data.entity.model.MobileModel
import com.example.data.entity.model.MobileModelDisplay

object MobileModelMapper {

    fun mapMobileListModelUI(mobileModelList: List<MobileModel>, favList: List<MobileModelDisplay>): List<MobileModelDisplay> {
        val mobileModelUiList = ArrayList<MobileModelDisplay>()
        for (mobileModel in mobileModelList) {
            mobileModelUiList.add(mapMobileModelUI(mobileModel, favList))
        }
        return mobileModelUiList
    }

    private fun mapMobileModelUI(mobileModel: MobileModel, favList: List<MobileModelDisplay>): MobileModelDisplay {
        val modelUi = MobileModelDisplay(brand = mobileModel.brand ?: "",
                thumbImageURL = mobileModel.thumbImageURL ?: "",
                price = mobileModel.price ?: "",
                name = mobileModel.name ?: "",
                rating = mobileModel.rating ?: "",
                id = mobileModel.id ?: "",
                description = mobileModel.description ?: "",
                isFav = false)
        for (favModel in favList) {
            if (modelUi.id == favModel.id) {
                modelUi.isFav = true
                break
            }
        }
        return modelUi
    }
}
