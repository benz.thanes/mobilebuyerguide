package com.example.data.entity.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class MobileDetailImageListModelDisplay(
        var id: String? = null,
        var mobile_id: String? = null,
        var url: String? = null
) : Parcelable
