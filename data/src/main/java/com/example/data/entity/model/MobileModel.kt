package com.example.data.entity.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class MobileModel(
        @SerializedName("brand") var brand: String? = null,
        @SerializedName("thumbImageURL") var thumbImageURL: String? = null,
        @SerializedName("price") var price: String? = null,
        @SerializedName("name") var name: String? = null,
        @SerializedName("rating") var rating: String? = null,
        @SerializedName("id") var id: String? = null,
        @SerializedName("description") var description: String? = null
) : Parcelable
