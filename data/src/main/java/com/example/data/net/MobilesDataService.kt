package com.example.data.net


import com.example.data.entity.model.MobileDetailImageListModel
import com.example.data.entity.model.MobileModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path


interface MobilesDataService {
    @GET("mobiles/")
    fun getMobilesData(): Observable<List<MobileModel>>

    @GET("mobiles/{id}/images/")
    fun getDetailImageList(@Path("id") id: String): Observable<List<MobileDetailImageListModel>>

}
