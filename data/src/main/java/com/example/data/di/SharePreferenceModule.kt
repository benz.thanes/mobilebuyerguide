package com.example.data.di

import android.app.Application
import com.example.data.cache.SharePrefMobilesFavorite
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class SharePreferenceModule {

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application): SharePrefMobilesFavorite {
        return SharePrefMobilesFavorite(application)
    }
}