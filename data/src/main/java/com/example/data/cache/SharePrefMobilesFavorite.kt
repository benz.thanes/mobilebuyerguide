package com.example.data.cache

import android.content.Context
import com.example.data.entity.model.MobileModelDisplay
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class SharePrefMobilesFavorite constructor(context: Context) {


    companion object {
        const val MOBILE_FAV = "MOBILE_FAV"

    }

    private val gson = Gson()
    private val prefs = context.getSharedPreferences(MOBILE_FAV, Context.MODE_PRIVATE)


    fun saveMobilesFavorite(list: ArrayList<MobileModelDisplay>) {
        val editor = prefs.edit()
        val json = gson.toJson(list)
        editor.putString(MOBILE_FAV, json)
        editor.apply()
    }

    fun getMobilesFavorite(): ArrayList<MobileModelDisplay> {
        val json = prefs.getString(MOBILE_FAV, null)
        val type = object : TypeToken<ArrayList<MobileModelDisplay>>() {
        }.type
        return if (json.isNullOrEmpty()) {
            ArrayList()
        } else {
            gson.fromJson(json, type)
        }
    }
}
