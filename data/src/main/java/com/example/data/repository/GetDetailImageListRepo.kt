package com.example.data.repository

import com.example.data.entity.model.MobileDetailImageListModelDisplay
import io.reactivex.Observable


interface GetDetailImageListRepo {

    fun getDetailImageList(id: String): Observable<List<MobileDetailImageListModelDisplay>>

}

