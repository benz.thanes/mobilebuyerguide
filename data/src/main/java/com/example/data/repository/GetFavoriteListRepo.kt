package com.example.data.repository

import com.example.data.entity.model.MobileModelDisplay

interface GetFavoriteListRepo {

    fun getFavoriteListRepo(): ArrayList<MobileModelDisplay>

}

