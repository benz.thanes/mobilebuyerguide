package com.example.data.repository

import com.example.data.entity.model.MobileModelDisplay


interface UpdateFavoriteListRepo {

    fun updateFavoriteListStorage(list: ArrayList<MobileModelDisplay>)

}

