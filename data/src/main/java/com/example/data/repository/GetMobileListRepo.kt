package com.example.data.repository

import com.example.data.entity.model.MobileModelDisplay
import io.reactivex.Observable


interface GetMobileListRepo {

    fun getMobileList(favList: ArrayList<MobileModelDisplay>): Observable<List<MobileModelDisplay>>

}

