package com.example.domain.usecase

import com.example.data.entity.model.MobileModelDisplay
import com.example.data.repository.UpdateFavoriteListRepo
import javax.inject.Inject


class UpdateFavoriteListUseCase @Inject constructor(private val repoImpl: UpdateFavoriteListRepo) {

    fun updateFavorite(list: ArrayList<MobileModelDisplay>) {
        return repoImpl.updateFavoriteListStorage(list)
    }

}