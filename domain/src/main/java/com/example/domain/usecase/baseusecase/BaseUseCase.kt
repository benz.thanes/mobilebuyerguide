package com.example.domain.usecase.baseusecase

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers


//abstract class BaseUseCase<T, in Params> : UseCase() {
//
//    internal abstract fun buildUseCaseSingle(params: Params): Observable<T>
//
//    fun execute (
//            onSuccess: ((t: T) -> Unit),
//            onError: ((t: Throwable) -> Unit),
//            params: Params
//    ) {
//        disposeLast()
//        lastDisposable = buildUseCaseSingle(params)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(onSuccess, onError)
//        lastDisposable?.let { compositeDisposable.add(it) }
//    }
//}

abstract class BaseUseCase<T, in Params> internal constructor() {
    private val disposables: CompositeDisposable = CompositeDisposable()

    internal abstract fun buildUseCaseObservable(params: Params): Observable<T>

    fun execute(observer: DisposableObserver<T>, params: Params) {

        val observable = this.buildUseCaseObservable(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        addDisposable(observable.subscribeWith(observer))
    }

    fun dispose() {
        if (!disposables.isDisposed) {
            disposables.dispose()
        }
    }

    private fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }
}