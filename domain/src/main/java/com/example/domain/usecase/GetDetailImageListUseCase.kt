package com.example.domain.usecase

import com.example.data.entity.model.MobileDetailImageListModelDisplay
import com.example.data.repository.GetDetailImageListRepo
import com.example.domain.usecase.baseusecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject


class GetDetailImageListUseCase @Inject constructor(private val repoImpl: GetDetailImageListRepo) :
        BaseUseCase<List<MobileDetailImageListModelDisplay>, String>() {
    override fun buildUseCaseObservable(params: String): Observable<List<MobileDetailImageListModelDisplay>> {
        return repoImpl.getDetailImageList(params)
    }

//    override fun buildUseCaseSingle(params: String): Observable<List<MobileDetailImageListModel>> {
//        return repoImpl.getDetailImageList(params)
//    }

}