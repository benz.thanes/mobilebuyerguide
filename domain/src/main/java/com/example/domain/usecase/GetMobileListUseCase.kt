package com.example.domain.usecase

import com.example.data.entity.model.MobileModelDisplay
import com.example.data.repository.GetMobileListRepo
import com.example.domain.usecase.baseusecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject


class GetMobileListUseCase @Inject constructor(private val repoImpl: GetMobileListRepo) :
        BaseUseCase<List<MobileModelDisplay>, ArrayList<MobileModelDisplay>>() {
    override fun buildUseCaseObservable(params: ArrayList<MobileModelDisplay>): Observable<List<MobileModelDisplay>> {
        return repoImpl.getMobileList(params)
    }

//    override fun buildUseCaseSingle(params: String): Observable<List<MobileModel>> {
//        return repoImpl.getMobileList()
//    }
}