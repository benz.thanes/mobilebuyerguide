package com.example.domain.usecase

import com.example.data.entity.model.MobileModelDisplay
import com.example.data.repository.GetFavoriteListRepo
import javax.inject.Inject


class GetFavoriteDataUseCase @Inject constructor(private val repoImpl: GetFavoriteListRepo) {
    fun getFavoriteData(): ArrayList<MobileModelDisplay> {
        return repoImpl.getFavoriteListRepo()
    }
}