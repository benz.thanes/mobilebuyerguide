package com.example.domain.di

import com.example.data.cache.SharePrefMobilesFavorite
import com.example.data.net.MobilesDataService
import com.example.data.repository.GetDetailImageListRepo
import com.example.data.repository.GetFavoriteListRepo
import com.example.data.repository.GetMobileListRepo
import com.example.domain.repositoryContract.GetDetailImageListRepoImpl
import com.example.domain.repositoryContract.GetFavoriteListRepoImpl
import com.example.domain.repositoryContract.GetMobileListRepoImpl
import com.example.domain.repositoryContract.UpdateFavoriteListRepoImpl
import com.example.domain.usecase.GetDetailImageListUseCase
import com.example.domain.usecase.GetFavoriteDataUseCase
import com.example.domain.usecase.GetMobileListUseCase
import com.example.domain.usecase.UpdateFavoriteListUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class UseCaseModule {

    @Singleton
    @Provides
    fun provideGetMobileRepo(dataService: MobilesDataService): GetMobileListRepo {
        return GetMobileListRepoImpl(dataService)
    }

    @Singleton
    @Provides
    fun provideGetMobileListUseCase(mobileListRepoImpl: GetMobileListRepoImpl): GetMobileListUseCase {
        return GetMobileListUseCase(mobileListRepoImpl)
    }

    @Singleton
    @Provides
    fun getDetailImageListRepo(dataService: MobilesDataService): GetDetailImageListRepo {
        return GetDetailImageListRepoImpl(dataService)
    }

    @Singleton
    @Provides
    fun provideGetDetailImageListUseCase(detailImageListRepo: GetDetailImageListRepoImpl): GetDetailImageListUseCase {
        return GetDetailImageListUseCase(detailImageListRepo)
    }

    @Singleton
    @Provides
    fun provideGetFavoriteDataListRepo(sharePref: SharePrefMobilesFavorite): GetFavoriteListRepo {
        return GetFavoriteListRepoImpl(sharePref)
    }

    @Singleton
    @Provides
    fun provideGetFavoriteDataListUseCase(getFavoriteListRepoImpl: GetFavoriteListRepoImpl): GetFavoriteDataUseCase {
        return GetFavoriteDataUseCase(getFavoriteListRepoImpl)
    }

    @Singleton
    @Provides
    fun provideUpdateFavoriteListRepo(sharePref: SharePrefMobilesFavorite): GetFavoriteListRepo {
        return GetFavoriteListRepoImpl(sharePref)
    }

    @Singleton
    @Provides
    fun provideUpdateFavoriteListUseCase(updateFavoriteListRepoImpl: UpdateFavoriteListRepoImpl): UpdateFavoriteListUseCase {
        return UpdateFavoriteListUseCase(updateFavoriteListRepoImpl)
    }


}