package com.example.domain.repositoryContract

import android.annotation.SuppressLint
import com.example.data.entity.mapper.MobileDetailModelMapper
import com.example.data.entity.model.MobileDetailImageListModelDisplay
import com.example.data.net.MobilesDataService
import com.example.data.repository.GetDetailImageListRepo
import io.reactivex.Observable
import javax.inject.Inject

class GetDetailImageListRepoImpl @Inject constructor(private val serviceApi: MobilesDataService) : GetDetailImageListRepo {
    @SuppressLint("CheckResult")
    override fun getDetailImageList(id: String): Observable<List<MobileDetailImageListModelDisplay>> {
        return serviceApi.getDetailImageList(id).concatMap { response ->
            return@concatMap Observable.just(MobileDetailModelMapper.mobileDetailImageListUI(ArrayList(response)))
        }
    }
}