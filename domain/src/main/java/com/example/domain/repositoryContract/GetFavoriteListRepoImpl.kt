package com.example.domain.repositoryContract

import com.example.data.cache.SharePrefMobilesFavorite
import com.example.data.entity.model.MobileModelDisplay
import com.example.data.repository.GetFavoriteListRepo
import javax.inject.Inject

class GetFavoriteListRepoImpl @Inject constructor(private val sharePrefMobilesFavorite: SharePrefMobilesFavorite) : GetFavoriteListRepo {
    override fun getFavoriteListRepo(): ArrayList<MobileModelDisplay> {
        return sharePrefMobilesFavorite.getMobilesFavorite()
    }
}