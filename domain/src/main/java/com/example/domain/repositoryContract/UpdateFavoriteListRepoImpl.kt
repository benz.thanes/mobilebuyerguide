package com.example.domain.repositoryContract

import com.example.data.cache.SharePrefMobilesFavorite
import com.example.data.entity.model.MobileModelDisplay
import com.example.data.repository.UpdateFavoriteListRepo
import javax.inject.Inject

class UpdateFavoriteListRepoImpl @Inject constructor(private val sharePrefMobilesFavorite: SharePrefMobilesFavorite) : UpdateFavoriteListRepo {

    override fun updateFavoriteListStorage(list: ArrayList<MobileModelDisplay>) {
        return sharePrefMobilesFavorite.saveMobilesFavorite(list)
    }

}