package com.example.domain.repositoryContract

import android.annotation.SuppressLint
import com.example.data.entity.mapper.MobileModelMapper
import com.example.data.entity.model.MobileModelDisplay
import com.example.data.net.MobilesDataService
import com.example.data.repository.GetMobileListRepo
import io.reactivex.Observable
import javax.inject.Inject

class GetMobileListRepoImpl @Inject constructor(private val serviceApi: MobilesDataService) : GetMobileListRepo {
    @SuppressLint("CheckResult")
    override fun getMobileList(favList: ArrayList<MobileModelDisplay>): Observable<List<MobileModelDisplay>> {
        return serviceApi.getMobilesData().concatMap { response ->
            return@concatMap Observable.just(MobileModelMapper.mapMobileListModelUI(ArrayList(response), favList))
        }
    }
}