package com.example.domain.usecase

import com.example.data.entity.model.MobileModelDisplay
import com.example.data.repository.UpdateFavoriteListRepo
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Test

class UpdateFavoriteListUseCaseTest {

    private var repo: UpdateFavoriteListRepo = mock()

    private lateinit var useCase: UpdateFavoriteListUseCase

    var mobileListModelList = ArrayList<MobileModelDisplay>()

    @Before
    fun setUp() {
        useCase = UpdateFavoriteListUseCase(repo)
        mobileListModelList.add(MobileModelDisplay(id = "1", rating = "4.9", price = "179"))
        mobileListModelList.add(MobileModelDisplay(id = "2", rating = "1.0", price = "179.99"))
        mobileListModelList.add(MobileModelDisplay(id = "3", rating = "5.0", price = "180"))
    }

    @Test
    fun updateFavorite() {
        useCase.updateFavorite(mobileListModelList)
        verify(repo).updateFavoriteListStorage(mobileListModelList)
    }
}

