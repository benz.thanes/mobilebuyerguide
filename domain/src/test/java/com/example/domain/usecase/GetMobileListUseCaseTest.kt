package com.example.domain.usecase

import com.example.data.entity.model.MobileModelDisplay
import com.example.data.repository.GetMobileListRepo
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GetMobileListUseCaseTest {

    private val getMobileListRepo: GetMobileListRepo = mock()

    private lateinit var useCase: GetMobileListUseCase
    var mobileListModelList = ArrayList<MobileModelDisplay>()
    @Before
    fun setUp() {
        useCase = GetMobileListUseCase(getMobileListRepo)
        mobileListModelList.add(MobileModelDisplay(id = "1", rating = "4.9", price = "179"))
        mobileListModelList.add(MobileModelDisplay(id = "2", rating = "1.0", price = "179.99"))
        mobileListModelList.add(MobileModelDisplay(id = "3", rating = "5.0", price = "180"))
    }

    @Test
    fun getMobileListService() {
        whenever(useCase.buildUseCaseObservable(mobileListModelList)).thenReturn(Observable.just(mobileListModelList))
        val modelFromServer = useCase.buildUseCaseObservable(mobileListModelList).blockingLast()
        Assert.assertNotNull(modelFromServer)
        Assert.assertEquals(modelFromServer.size, mobileListModelList.size)
        Assert.assertNotEquals(modelFromServer.size, 5)
    }
}