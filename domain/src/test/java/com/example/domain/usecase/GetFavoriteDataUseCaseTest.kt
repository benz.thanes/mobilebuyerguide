package com.example.domain.usecase

import com.example.data.entity.model.MobileModelDisplay
import com.example.data.repository.GetFavoriteListRepo
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GetFavoriteDataUseCaseTest {

    private val repo: GetFavoriteListRepo = mock()

    private lateinit var useCase: GetFavoriteDataUseCase
    var mobileListModelList = ArrayList<MobileModelDisplay>()
    @Before
    fun setUp() {
        useCase = GetFavoriteDataUseCase(repo)
        mobileListModelList.add(MobileModelDisplay(id = "1", rating = "4.9", price = "179"))
        mobileListModelList.add(MobileModelDisplay(id = "2", rating = "1.0", price = "179.99"))
        mobileListModelList.add(MobileModelDisplay(id = "3", rating = "5.0", price = "180"))
    }

    @Test
    fun getFavoriteData() {
        whenever(useCase.getFavoriteData()).thenReturn(mobileListModelList)
        val modelFromServer = useCase.getFavoriteData()
        Assert.assertNotNull(modelFromServer)
        Assert.assertEquals(modelFromServer.size, mobileListModelList.size)
        Assert.assertNotEquals(modelFromServer.size, 5)
    }
}