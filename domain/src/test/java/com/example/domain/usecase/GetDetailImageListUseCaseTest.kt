package com.example.domain.usecase

import com.example.data.entity.model.MobileDetailImageListModelDisplay
import com.example.data.repository.GetDetailImageListRepo
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GetDetailImageListUseCaseTest {

    private val getDetailImageListRepo: GetDetailImageListRepo = mock()

    private lateinit var useCase: GetDetailImageListUseCase
    private var mobileDetailImageListModelMapDisplay = ArrayList<MobileDetailImageListModelDisplay>()
    @Before
    fun setUp() {
        useCase = GetDetailImageListUseCase(getDetailImageListRepo)
        mobileDetailImageListModelMapDisplay.add(MobileDetailImageListModelDisplay(id = "1", mobile_id = "1", url = "https://www.91-img.com/gallery_images_uploads/f/c/fc3fba717874d64cf15d30e77a16617a1e63cc0b.jpg"))
        mobileDetailImageListModelMapDisplay.add(MobileDetailImageListModelDisplay(id = "6", mobile_id = "1", url = "https://www.91-img.com/gallery_images_uploads/b/4/b493185e7767c2a99cfeef712b11377f625766f2.jpg"))
        mobileDetailImageListModelMapDisplay.add(MobileDetailImageListModelDisplay(id = "7", mobile_id = "1", url = "https://www.91-img.com/gallery_images_uploads/c/3/c32cff8945621ad06c929f50af9f7c55f978c726.jpg"))
    }

    @Test
    fun getMobileListService() {
        whenever(useCase.buildUseCaseObservable("1")).thenReturn(Observable.just(mobileDetailImageListModelMapDisplay))
        val modelFromServer = useCase.buildUseCaseObservable("1").blockingLast()
        Assert.assertNotNull(modelFromServer)
        Assert.assertEquals(modelFromServer.size, mobileDetailImageListModelMapDisplay.size)
        Assert.assertNotEquals(modelFromServer.size, 5)
    }
}

