package com.example.domain.repositoryContract

import com.example.data.entity.model.MobileModel
import com.example.data.entity.model.MobileModelDisplay
import com.example.data.net.MobilesDataService
import com.example.data.repository.GetMobileListRepo
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class GetMobileListRepoImplTest {

    private lateinit var repo: GetMobileListRepo

    private val serviceApi: MobilesDataService = mock()
    var mobileListModelList = ArrayList<MobileModel>()
    var mobileListModelListDisplay = ArrayList<MobileModelDisplay>()
    @Before
    fun setUp() {
        repo = GetMobileListRepoImpl(serviceApi)

        mobileListModelList.add(MobileModel(id = "1", rating = "4.9", price = "179"))
        mobileListModelList.add(MobileModel(id = "2", rating = "1.0", price = "179.99"))
        mobileListModelList.add(MobileModel(id = "3", rating = "5.0", price = "180"))

        //mock fav
        mobileListModelListDisplay.add(MobileModelDisplay(id = "1", rating = "4.9", price = "179", isFav = true, name = "", brand = "", thumbImageURL = "", description = ""))
        mobileListModelListDisplay.add(MobileModelDisplay(id = "2", rating = "1.0", price = "179.99", isFav = true, name = "", brand = "", thumbImageURL = "", description = ""))
        mobileListModelListDisplay.add(MobileModelDisplay(id = "3", rating = "5.0", price = "180", isFav = true, name = "", brand = "", thumbImageURL = "", description = ""))

    }

    @Test
    fun getMobileList() {
        whenever(serviceApi.getMobilesData()).thenReturn(Observable.just(mobileListModelList))
        val dataFromServer = repo.getMobileList(mobileListModelListDisplay).blockingFirst()
        assertEquals(dataFromServer, mobileListModelListDisplay)

    }
}