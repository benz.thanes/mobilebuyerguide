package com.example.domain.repositoryContract

import com.example.data.cache.SharePrefMobilesFavorite
import com.example.data.entity.model.MobileModelDisplay
import com.example.data.repository.GetFavoriteListRepo
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class GetFavoriteListRepoImplTest {

    private lateinit var repo: GetFavoriteListRepo
    private val pref: SharePrefMobilesFavorite = mock()

    var mobileListModelList = ArrayList<MobileModelDisplay>()
    @Before
    fun setUp() {
        repo = GetFavoriteListRepoImpl(pref)
        mobileListModelList.add(MobileModelDisplay(id = "1", rating = "4.9", price = "179"))
        mobileListModelList.add(MobileModelDisplay(id = "2", rating = "1.0", price = "179.99"))
        mobileListModelList.add(MobileModelDisplay(id = "3", rating = "5.0", price = "180"))
    }

    @Test
    fun getFavoriteListRepo() {
        whenever(pref.getMobilesFavorite()).thenReturn(mobileListModelList)
        val dataFromServer = repo.getFavoriteListRepo()
        assertEquals(dataFromServer, mobileListModelList)
    }
}