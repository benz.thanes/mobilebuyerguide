package com.example.domain.repositoryContract

import com.example.data.cache.SharePrefMobilesFavorite
import com.example.data.entity.model.MobileModelDisplay
import com.example.data.repository.UpdateFavoriteListRepo
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Test

class UpdateFavoriteListRepoImplTest {

    private lateinit var repo: UpdateFavoriteListRepo
    private val pref: SharePrefMobilesFavorite = mock()

    var mobileListModelList = ArrayList<MobileModelDisplay>()
    @Before
    fun setUp() {
        repo = UpdateFavoriteListRepoImpl(pref)
        mobileListModelList.add(MobileModelDisplay(id = "1", rating = "4.9", price = "179"))
        mobileListModelList.add(MobileModelDisplay(id = "2", rating = "1.0", price = "179.99"))
        mobileListModelList.add(MobileModelDisplay(id = "3", rating = "5.0", price = "180"))
    }

    @Test
    fun updateFavoriteListStorage() {
        repo.updateFavoriteListStorage(mobileListModelList)
        verify(pref).saveMobilesFavorite(mobileListModelList)
    }
}